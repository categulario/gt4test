use gtk::prelude::*;
use gtk::{
    Application, ApplicationWindow, Button, EventControllerScroll, Inhibit,
    EventControllerScrollFlags,
};
use gdk::ScrollEvent;

fn main() {
    // Create a new application
    let app = Application::builder()
        .application_id("org.gtk-rs.example")
        .build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run();
}

fn build_ui(app: &Application) {
    // Create a button with label and margins
    let button = Button::builder()
        .label("Press me!")
        .margin_top(12)
        .margin_bottom(12)
        .margin_start(12)
        .margin_end(12)
        .build();

    // Connect to "clicked" signal of `button`
    button.connect_clicked(move |button| {
        // Set the label to "Hello World!" after the button has been clicked on
        button.set_label("Hello World!");
    });

    let controller = EventControllerScroll::new(EventControllerScrollFlags::BOTH_AXES);

    button.add_controller(&controller);

    controller.connect_scroll(|controller, b, c| {
        if let Some(event) = controller.current_event() {
            let se: ScrollEvent = event.downcast().unwrap();

            if let Some(device) = se.device() {
                dbg!(device.source());
            }
        }

        Inhibit(false)
    });

    // Create a window
    let window = ApplicationWindow::builder()
        .application(app)
        .title("My GTK App")
        .child(&button)
        .build();

    // Present window
    window.present();
}
